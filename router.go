package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

func router(w http.ResponseWriter, r *http.Request) {

	regHandleAPI, err := regexp.Compile("^/igcinfo/api/?$")
	if err != nil {
		handleError(w, r, err, http.StatusBadRequest)
		return
	}

	fmt.Println(r.URL.Path)

	switch {
	case regHandleAPI.MatchString(r.URL.Path):
		hdlNum(w, r)
	default:

	}
}

func handleError(w http.ResponseWriter, r *http.Request, err error, status int) {
	if err != nil {
		http.Error(w, fmt.Sprintf("%s/t%s", http.StatusText(status), err), status)
	} else {
		http.Error(w, fmt.Sprintf(http.StatusText(status)), status)
	}
}

func hdlNum(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received on numeric path handler ...")
	content, _ := ioutil.ReadAll(r.Body)
	fmt.Println(string(content))
}

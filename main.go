package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", router)
	//http.HandleFunc("/subpath", hdlSub)

	fmt.Println(os.Getenv("PORT") + "!")
	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}
